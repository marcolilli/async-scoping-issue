const foo = 'foo'
console.log('foo', foo)

const asyncFunc = async () => {
    if (false) {
        const foo = 'bar'
        console.log('foo in if', foo)
    } else {
        console.log('foo in else', foo)
    }
}

asyncFunc()
